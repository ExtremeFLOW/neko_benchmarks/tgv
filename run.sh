#!/bin/bash

export CUDA_VISIBLE_DEVICES=${SLURM_LOCALID}

echo " MPI rank: ${SLURM_PROCID} | CUDA Device: ${CUDA_VISIBLE_DEVICES} "

"$@"


